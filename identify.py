import glob

import pandas as pd
import numpy as np

asset_reports_dir = 'asset_reports'
allFiles = glob.glob(asset_reports_dir + "/*.csv")
frame = pd.DataFrame()
list_ = []
for file_ in allFiles:
    df = pd.read_csv(file_, index_col=None, header=0, dtype=np.str).fillna('')
    list_.append(df)
df_report = pd.concat(list_)
df_report.rename(inplace=True, columns={
    'HFA Song Code': 'hfa_song_code',
    'ISWC': 'iswc',
    'Asset ID': 'asset_id'
})
df_report.drop_duplicates('asset_id', inplace=True)

df_settlement = pd.read_csv(
    'settlement_investigate.csv', dtype=np.str)

iswc_merged = pd.merge(left=df_report, right=df_settlement, on='iswc', indicator=True, how='outer')
hfa_merged = pd.merge(left=df_report, right=df_settlement, on='hfa_song_code', indicator=True, how='outer')

found_iswc_df = iswc_merged[iswc_merged['_merge'] == 'both']
found_hfa_df = hfa_merged[hfa_merged['_merge'] == 'both']


found_df = pd.concat([found_iswc_df, found_hfa_df])

investigate = []
iswcs = [x for x in list(set(list(found_df['iswc']))) if str(x) != 'nan']
hfas = [x for x in list(set(list(found_df['hfa_song_code']))) if str(x) != 'nan']

difference = df_settlement[(~df_settlement.iswc.isin(iswcs)) &(~df_settlement.hfa_song_code.isin(hfas))]
difference.to_csv('investigate.csv', index=False)
found_df.to_csv('to_ingest_directly.csv', index=False
                )